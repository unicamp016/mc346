# Ex. 1 - Criar uma lista com apenas os elementos pares de outra
def pares(lista):
    return [x for x in lista if x%2 == 0]

# Ex. 2 - Criar uma lista com os valores nas posicoes pares
def pos_pares(lista):
    return [x for i, x in enumerate(lista) if i%2 == 0]

# Ex. 3 - Criar um dicionário com a contagem de cada elemento em uma lista
def criar_dic(lista):
    dic = {}
    for x in lista:
        if x in dic:    dic[x] += 1
        else:           dic[x] = 1
    return dic

# Ex. 4 - Chave associada ao maior valor de um dicionário
def maior_chave(dic):
    key, max = dic.popitem()
    for k in dic.keys():
        if dic.get(k) > max:
            max = dic.get(k)
            key = k
    return key

# Ex. 5 - Qual o elemento mais comum numa lista
def mais_comum(lista):
    return maior_chave(criar_dic(lista))

# Ex. 6 - Uma lista é sublista de outra?
def is_sublista(lista1, lista2):
    return lista1 in lista2

def is_sublista2(sublista, lista):
    for i in range(len(lista)-len(sublista)+1):
        if lista[i:i+len(sublista)] == sublista:
            return True
    return False

# Ex. 7 - Dadas duas strings, verifica se o fim de uma é igual ao começo de outra.
def is_end_prefix(string1, string2):
    for i in range(len(string1)):
        end = string1[i:]
        if string2.find(end) == 0:
            return (True, end)
    return (False, None)

import numpy as np

# Ex. 1 - Normalize as linhas de forma que a soma dos quadrados das linhas seja 1
def normalize_row(M):
    # Pelas regras de broadcasting, devemos ter que
    # M    (2d array):  m x n
    # D    (2d array):  m x 1
    D = np.sqrt(np.sum(M**2, axis=1))[:,np.newaxis]
    return M/D

# Ex. 2 - Normalize as colunas de forma que a soma dos quadrados das colunas seja 1
def normalize_col(M):
    # Pelas regras de broadcasting, devemos ter que
    # M    (2d array):  m x n
    # d    (1d array):      n
    d = np.sqrt(np.sum(M**2, axis=0))
    return M/D

# Ex. 3 - Soma dos elementos das linhas cujo elemento da primeira coluna é maior que 0
def add_positive_rows(M):
    # Pelas regras de broadcasting, devemos ter que
    # M    (2d array):  m x n
    # B    (2d array):  m x 1
    b = M[:,0] > 0      # Boolean array of dimension m
    A = M[b]
    return np.sum(A, axis=1)

# Ex. 4 - Compute um array com o item de menor valor absoluto de cada coluna
def minimum_abs_cols(M):
    A = np.abs(M)
    r = np.argmin(A, axis=0)
    c = np.arange(M.shape[1])
    return M[r,c]

# Ex. 5 - Dado um array 1D, troque os valores > 0 para 1 e < 0 para -1
def reduce_abs(A):
    A[A < 0] = -1
    A[A > 0] = 1
    return A

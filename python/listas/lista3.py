import time
import functools

# Ex. 1 - Decorator para imprimir o tempo de execução
def time_exec(func):
    def timer(*args, **kwargs):
        start = time.time()
        val = func(*args, **kwargs)
        print (f"Finished executing {func.__name__} in {time.time()-start:.4f} secs")
        return val
    return timer

# Ex. 2 - Decorator para constuir uma string com linhas para a hora e argumentos e saida de cada chamada da funcao.
#         O string será acessado via atributo
def debug(func):
    def debugger(*args, **kwargs):
        params = f"Calling {func.__name__} with args = {args} and kwargs = {kwargs}"
        print (params)
        val = func(*args, **kwargs)
        ret = f"{func.__name__} returned {val}"
        print(ret)
        debugger.call_info = "\n".join([params, ret])
        return val
    debugger.call_info = "No calls have been made yet\n"
    return debugger

# Ex. 3 - Decorator para memoizar a função. Memoizar é lembrar dos valores de entrada e saída da função já executada.
class Memoize:
    def __init__(self, func):
        self.func = func
        self.prev_calls = {}
    def __call__(self, *args, **kwargs):
        call_id = str(args) + ", " + str(kwargs)
        if call_id in prev_calls:
            return prev_calls["call_id"]
        val = func(*args, **kwargs)
        dict.append((call_id, val))
        return val

# Python primer's version
def cache(func):
    """Keep a cache of previous function calls"""
    @functools.wraps(func)
    def wrapper_cache(*args, **kwargs):
        cache_key = args + tuple(kwargs.items())
        if cache_key not in wrapper_cache.cache:
            wrapper_cache.cache[cache_key] = func(*args, **kwargs)
        return wrapper_cache.cache[cache_key]
    wrapper_cache.cache = dict()
    return wrapper_cache

# Ex. 4 - Decorator para log argumentos e horário num arquivo (append) dado como argumento do decorator
def log(arquivo):
    def decorator_log(func):
        def wrapper_log(*args, **kwargs):
            func_args = parse_args(*args, **kwargs)
            time_str = time.strftime("[%d/%m/%y - %H:%M:%S]")
            with open(arquivo, 'a') as f:
                f.write(f"{time_str} Calling {func.__name__}({func_args})\n")
            return func(*args, **kwargs)
        return wrapper_log
    return decorator_log

# Teste - Decorator para fazer um log das chamadas de uma função
def logstr(func):
    def wrapper_logstr(*args, **kwargs):
        func_args = parse_args(*args, **kwargs)
        val = func(*args, **kwargs)
        f.log += f"{time.asctime()} entrada: ({func_args}) saida: {val}\n"
        return val
    wrapper_logstr.log = ''
    return wrapper_logstr

# Teste - imprimir distribuição dos primeiros dígitos dos números
def benford(entrada):
    entrada = entrada.split()
    first_num = [e[0] for e in entrada if e[0].isdigit()]
    print(first_num)
    print("Proporção de dígitos:")
    for i in range(1, 10):
        prop = first_num.count(str(i))/len(first_num)
        print(f"{i} - {prop}")

# Util
def parse_args(*args, **kwargs):
    args_list = [f"{a!r}" for a in args]
    kwargs_list = [f"{k}={v!r}" for k, v in kwargs.items()]
    return ", ".join(args_list + kwargs_list)

# Test functions
@logstr
def f(a, b):
    return 2*a+b

@repeat(num_times=4)
@log(arquivo='log.txt')
def greet(name):
    print(f"Hello {name}")

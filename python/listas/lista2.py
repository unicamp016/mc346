# Ex. 1 - Dado um iterator, retorna um iterator com os elementos nas posicoes pares
def pares(it):
    return (x for i, x in enumerate(it) if i%2 == 0)

def pares2(it):
    while True:
        yield next(it)
        _ = next(it)

# Ex. 2 - Dado um iterator, reverte ele.
def reverte(it):
    l = list(it)
    l.reverse()
    return (x for x in l)

# Ex. 3 - Dado 2 iterators, retorna um iterator com os elementos intercalados
def intercala(iter1, iter2):
    while True:
        yield next(iter1)
        yield next(iter2)

# Ex. 4 - Produto cartesiano entre dois iterators
def cartesian(iter1, iter2):
    l = list(iter2)
    for x in iter1:
        iter2 = iter(l)
        for y in iter2:
            yield (x, y)

# Ex. 5 - Dado um iterator retorna os elementos num ciclo infinito
def ciclo(it):
    l = list(it)
    while True:
        try:
            yield next(it)
        except StopIteration:
            it = iter(l)

# Ex. 6 - Retorna um iterator que gera números de init até infinito
def range_inf(init, passo=1):
    class InfIterator:
        def __init__(self, init, passo=1):
            self.init = init-passo
            self.passo = passo
        def __iter__(self):
            return self
        def __next__(self):
            self.init += passo
            return self.init
    return InfIterator(init, passo)

def range_inf2(init, passo=1):
    while True:
        yield init
        init += passo

# Ex. 7 - Os primeiros n elementos da lista (ou iterator)
def take(lista, n):
    return lista[:n]

def take_iter(it, n):
    for i in range(n):
        yield next(it)
    raise StopIteration

# Ex. 8 - A lista (ou iterator) sem os n primeiros
def drop(lista, n):
    return lista[n:]

def drop_iter(it, n):
    for i in range(n):
        _ = next(it)
    while True:
        yield next(it)

# Teste - Iterator que elimina elementos subsequentes repitidos
def filtra_rep(iter1):
    x = next(iter1)
    while True:
        yield x
        y = next(iter1)
        while x == y:
            y = next(iter1)
        x = y

def filtra_rep2(iter1):
    prev = None
    while True:
        x = next(iter1)
        if prev != x:
            yield x
        prev = x

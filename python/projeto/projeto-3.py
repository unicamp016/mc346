# Nome: Rafael Figueiredo Prudencio
# RA: 186145

import time
import sys
import numpy as np
from itertools import combinations

class Uber:
    def __init__(self, edges):
        # Find number of vertices in graph
        n = 0
        for e in edges:
            i, j = tuple(e.split()[:2])
            max_v = max(int(i), int(j))
            if max_v > n:
                n = max_v

        # Initialize edge costs
        self.W = np.full((n+1, n+1), float("inf"), dtype=np.float32)
        for i in range(n+1):
            self.W[i,i] = 0

        # Update edge costs with known values
        for e in edges:
            i, j, w = tuple(e.split())
            i, j, w = int(i), int(j), float(w)
            self.W[i, j] = w

    def find_pool_rides(self, trips):
        # Initialize dictionary of unpaired trips.
        cur_trips = {}
        for p, t in enumerate(trips):
            cur_trips[p+1] = tuple(map(int, t.split()))

        pool_trips = {}
        for i, j in combinations(cur_trips.keys(), r=2):
            trip1, trip2 = cur_trips[i], cur_trips[j]
            if len(trip1) + len(trip2) == 6:
                continue
            pool_trips[(i, j)] = self.lowest_max(trip1, trip2)

        pool_trips = sorted(pool_trips.items(), key=lambda t: t[1][0])

        while pool_trips:
            (p1, p2), (incv, path) = pool_trips[0]

            # In case all remaining trips have an unacceptable inconvenience
            if incv == float('inf'):
                break

            # We wish to print passenger list in the same pickup order
            if cur_trips[p2][0] == path[0]:
                p1, p2 = p2, p1

            # Update dict of current trips that should be assigned
            cur_trips.pop(p1)
            cur_trips.pop(p2)

            # Update uber pool trip candidates
            pool_trips = [(k,v) for k,v in pool_trips if p1 != k[0] and p1 != k[1] and p2 != k[0] and p2 != k[1]]

            print (f"passageiros: {p1} {p2} percurso:  {path[0]} {path[1]} {path[2]} {path[3]}")

        # Print trajectory for all passengers that didn't find a pool partner
        for k, v in cur_trips.items():
            if len(v) == 2:
                a, b = v[0], v[1]
                if self.W[a,b] == float('inf'):
                    print(f"passageiro: {k} percurso: a viagem não pode ser completada")
                else:
                    print(f"passageiro: {k} percurso: {v[0]} {v[1]}")
            else:
                a, b = v[2], v[1]
                if self.W[a,b] == float('inf'):
                    print(f"passageiro: {k} percurso: a viagem não pode ser completada")
                else:
                    print(f"passageiro: {k} percurso: {v[2]} {v[1]}")


    def lowest_max(self, trip1, trip2):
        T = self.W
        if len(trip1) + len(trip2) == 4:
            a, b = trip1
            c, d = trip2

            if T[a,b] == float('inf') or T[c,d] == float('inf'):
                return float('inf'), None

            p1 = max((T[a,c]+T[c,d]+T[d,b])/T[a,b], 1)
            p2 = max((T[a,c]+T[c,b])/T[a,b], (T[c,b]+T[b,d])/T[c,d])
            p3 = max((T[a,d]+T[d,b])/T[a,b], (T[c,a]+T[a,d])/T[c,d])
            p4 = max(1, (T[c,a]+T[a,b]+T[b,d])/T[c,d])
            p = min(p1, p2, p3, p4)

            if p > 1.4: return float('inf'), None
            if p == p1: return p, (a,c,d,b)
            if p == p2: return p, (a,c,b,d)
            if p == p3: return p, (c,a,d,b)
            if p == p4: return p, (c,a,b,d)
        else:
            if len(trip1) == 3:
                a, b, x = trip1
                c, d = trip2
            else:
                a, b, x = trip2
                c, d = trip1

            if T[x,b] == float('inf') or T[c,d] == float('inf'):
                return float('inf'), None

            p1 = max((T[x,c]+T[c,b])/T[x,b], (T[c,b]+T[b,d])/T[c,d])
            p2 = max((T[x,c]+T[c,d]+T[d,b])/T[x,b], 1)
            p = min(p1, p2)

            if p > 1.4:     return float('inf'), None
            if p == p1:     return p, (x,c,b,d)
            if p == p2:     return p, (x,c,d,b)

    def compute_optimal_paths(self):
        # Floyd-Warshall implementation to compute minimal time cost betwen all vertices
        D = self.W
        n = D.shape[0]
        for k in range(n):
            D_k = np.empty((n,n), dtype=np.float32)
            for i in range(n):
                for j in range(n):
                    D_k[i,j] = min(D[i,j], D[i,k]+D[k,j])
            D = D_k
        self.W = D

    def print_graph(self):
        print(self.W)


if __name__ == "__main__":
    # Make sure we always print full numpy array
    np.set_printoptions(threshold=np.nan)

    # Read file until EOF
    input_fn = sys.argv[1]
    with open(input_fn, "r") as f:
        input = f.read()

    # Split input into graph data and requests data
    graph, trips = tuple(input.split("\n\n"))
    edges = graph.strip().split('\n')
    trips = trips.strip().split('\n')

    # Initialize Uber object with initial graph
    uber = Uber(edges)
    uber.compute_optimal_paths()
    uber.find_pool_rides(trips)

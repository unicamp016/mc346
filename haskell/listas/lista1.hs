-- Ex. 1
tamanho [] = 0
tamanho x = 1 + tamanho (tail x)

tamanho' x = if x == [] then 0
                        else 1 + tamanho' (tail x)

tamanho'' x
            | x == [] = 0
            | otherwise = 1 + tamanho'' (tail x)

--Ex. 2
somaElementos x = if x == [] then 0 else head x + somaElementos (tail x)

--Ex. 3
somaPares x = if x == []
                then 0
                else if (head x) `mod` 2 == 0
                  then head x + somaPares (tail x)
                  else somaPares (tail x)

--Ex. 4
somaPosicoesPares [] = 0
somaPosicoesPares x = head (tail x) + somaPosicoesPares (tail (tail x))

-- somaPosicoesPares (x1:x2:xs)

--Ex. 5
existe x [] = False
existe x l = if x == head l then True
             else existe x (tail l)

existe' it [] = False
existe' it (x:xs) = if x == it then True
                    else existe' it xs


--Ex. 6 Posicao do item na lista (0 se não está e 1 se for o primeiro)
position x [] = 0
position x l = if not (existe x l) then 0
               else 1 + position x (tail l)

position' i [] = 0
position' i (x:xs)
    | not (existe i (x:xs)) = 0
    | otherwise = 1 + position' i xs

--Ex. 7
contar x [] = 0
contar x l = if x == head l then 1 + contar x (tail l)
                      else 0 + contar x (tail l)

--Ex. 8
-- maior [x] = x
-- maior (x:xs) = if x >= maior xs then x
--                               else maior xs
--
-- maior' [x] = x
-- maior' (x:xs) = let
--           ma = maior' xs
--           in if x > ma then x else ma

maiorExp [x] = x
maiorExp (x:xs) = if x > maiorExp xs then x else maiorExp xs

maior2 [x] = x
maior2 (x:xs) = let mm = maior2 xs in
                if x > mm then x else mm


maior' [] a = a
maior' (y:ys) a
   | y>a = maior' ys y
   | otherwise = maior' ys a

maior (x:xs) = maior' xs x


--Ex.9
revert l = if l == [] then []
                      else (returnLast l):revert (returnInit l)

revert' (x:xs)
    | xs == [] = [x]
    | otherwise = revert' xs ++ [x]

revert2 l = reverte2' l []
            where reverte2' [] acc = acc
                  reverte2' (x:xs) acc = reverte2' xs (x:acc)

--Ex. 10
intercala l1 [] = l1
intercala [] l2 = l2
intercala l1 l2 = [head l1] ++ [head l2] ++ intercala (tail l1) (tail l2)

--Ex. 11
isSorted [] = True
isSorted [x] = True
isSorted l = if head l <= head (tail l) then isSorted (tail l)
             else False

--Ex. 12
range' i f = if i > f then []
                     else [i] ++ range' (i+1) f

range n = range' 1 n

range'' 0 = []
range'' n = range'' (n-1) ++ [n]

--Ex. 13
returnLast l = if tail l == [] then head l
                               else returnLast (tail l)

--Ex. 14
returnInit l = if tail l == [] then []
                               else [(head l)] ++ returnInit (tail l)
--Ex 15
shiftr l = [returnLast l] ++ returnInit l

--Ex 16
shiftrn' i f l = if i == f then l
                           else shiftrn' (i+1) f (shiftr l)
shiftrn n l = shiftrn' 0 n l

--Ex. 17
removeItemOnce' i l = if i == head l then tail l
                                    else [(head l)] ++ removeItemOnce i (tail l)
removeItemOnce i l = if existe i l then removeItemOnce' i l
                                   else l
--Ex. 18
removeAllItems i l = if existe i l
                        then removeAllItems i (removeItemOnce i l)
                        else l

--Ex. 19
removeItemN' s f i l = if s > f then l
                                else removeItemN' (s+1) f i (removeItemOnce i l)
removeItemN n i l = removeItemN' 1 n i l

-- removeN 0 i l = []
-- removeN n i (x:xs) = if x == i then removeN n-1 i xs
--                                else x:removeN n i xs

--Ex. 20
removeLastItem i l = if not (existe i l) then l
                     else revert (removeItemOnce i (revert l))

--Ex. 21
exchangeItem old new (x:xs) = if x == old then new:xs
                                          else [x] ++ exchangeItem old new xs

--Ex. 22
exchangeAllItems old new [] = []
exchangeAllItems old new (x:xs) = if x == old then new:exchangeAllItems old new xs
                                              else x:exchangeAllItems old new xs

--Ex. 23
exchangeNItems n old new [] = []
exchangeNItems 0 old new l = l
exchangeNItems n old new (x:xs) = if x == old then new:exchangeNItems (n-1) old new xs
                                              else x:exchangeNItems n old new xs

trocaUltimo _ _ [] = ([], False)
trocaUltimo old new (x:xs) =
  let (xxs, trocou) = trocaUltimo old new xs in
  if not trocou && x == old then (new:xxs, True)
                            else (x:xxs, trocou)

-- trocaVelhorPorNovo velho novo [] = []
trocaVelhoPorNovo velho novo (x:xs)
  | (x:xs) == [] = []
  | velho == x = novo:(trocaVelhoPorNovo velho novo xs)
  | otherwise = x:(trocaVelhoPorNovo velho novo xs)

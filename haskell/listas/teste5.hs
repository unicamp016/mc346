-- soma1 :: (Eq a) => a -> [(a,Int)] -> [(a,Int)]
import Data.Char

soma1 x [] = [(x, 1)]
soma1 x (t:ts) = if x == fst t
                    then ((fst t), ((snd t) + 1)):ts
                    else t:(soma1 x ts)

vogais = ['a', 'e', 'i', 'o', 'u']
insertT str = foldl (\acc c -> soma1 (toLower c) acc) [] str

extraiVogais ts = filter (\t -> elem (fst t) vogais) ts

pegarMaior ts = foldl1 (\acc t -> if snd acc < snd t then t else acc) ts

vogalMaisComum s = fst $ pegarMaior $ extraiVogais $ insertT s


-- vogalmaiscomum s =

-- soma1 "abc" [("efg",4),("abc",2),("qwe",1)] ==> [("efg",4),("abc",3),("qwe",1)]
-- soma1 'a' [('b',1),('d',3),('u',2)] ==>  [('a',1),('b',1),('d',3),('u',2)]

factorial :: Int -> Int
factorial n = product [1..n]

--params str1 str2 str1' str2' acc ret
-- str1 - string original que queremos separar
-- str2 - substring que queremos encontrar
-- str1' - string atual que queremos separar
-- str2' - substring atual que qeremos encontrar
-- acc - acumulador para cada substring gerada na separacao
-- ret - lista com todas as substrings geradas

batata = 4

-- split :: [Char] -> [Char] -> [[Char]]
split str1 str2 = split' str1 str2 str1 str2 [] []
    where split' str1 str2 [] [] acc ret = ret ++ [acc]
          split' str1 str2 [] str2' acc ret = ret ++ [acc++str1]
          split' str1 str2 str1' [] acc ret = split' str1' str2 str1' str2 [] (ret ++ [acc])
          split' (x:xs) (y:ys) (x':x's) (y':y's) acc ret =
            if x == y -- encontramos um possível local de separacao
              then
                if x' == y'
                  then split' (x:xs) (y:ys) x's y's acc ret
                  else split' xs (y:ys) xs (y:ys) (acc ++ [x]) ret
              else split' xs (y:ys) xs (y:ys) (acc ++ [x]) ret


splitseq sep str = splitseq' sep str sep str [] []
  where splitseq' sep str [] [] ac ret = ret ++ [ac]
        splitseq' sep str [] str' ac ret = split' sep str' sep str'

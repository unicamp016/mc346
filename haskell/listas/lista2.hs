-- posicoes - dado um item e uma lista, retorna uma lista
-- com todas as posicoes (primeiro elemento esta na posicao 1) do item na lista

positions it l = positions' it l [] 1
                 where positions' _ [] acc _ = acc
                       positions' it (x:xs) acc i =
                         if it == x then positions' it xs (acc ++ [i]) (i+1)
                                    else positions' it xs acc (i+1)

-- split - dado um item e uma lista retorna uma lista de listas, todos os elementos
-- da lista antes do item (a primeira vez que ele aparece) e todos depois

split l it = split' l [] it
             where split' [] acc it = [acc]
                   split' (x:xs) acc it =
                     if x == it then [acc, xs]
                     else split' xs (acc ++ [x]) it

splitAll l it = splitAll' l [] [] it
                where splitAll' [] [] ret _ = ret
                      splitAll' [] acc ret _ = ret ++ [acc]
                      splitAll' (x:xs) acc ret it =
                        if x == it then splitAll' xs [] (ret ++ [acc]) it
                                   else splitAll' xs (acc ++ [x]) ret it

-- A lista sem os primeiors n elementos
dropN (x:xs) n = if n == 1 then xs
                          else dropN xs (n-1)

-- Covers edge cases of n = 0 and n > len(l)
dropN' n (x:xs)
    | n == 0 = (x:xs)
    | null xs && n > 0 = []
    | otherwise = dropN' (n-1) xs

-- Os primeiros N elementos da listas
takeN (x:xs) n = if n == 1 then [x]
                           else x:(takeN xs (n-1))

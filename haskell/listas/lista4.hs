data Arvore a = Vazio | No a (Arvore a) (Arvore a) deriving (Eq, Show, Read)

s1 = No "a" Vazio (No "b" Vazio Vazio)
s2 = No 3 Vazio (No 5 Vazio Vazio)
s3 = No 5 (No 3 (No 1 Vazio Vazio ) (No 4 Vazio Vazio)) (No 7 (No 6 Vazio Vazio) (No 8 Vazio Vazio))
-- Acha um item numa arvore de busca binaria
findItem x Vazio = False
findItem x (No y left right)
  | x == y = True
  | x < y = findItem x left
  | x > y = findItem x right

verifyBST Vazio = True
verifyBST (No x Vazio Vazio) = True
verifyBST (No x left right)
  | x > maior left && x < menor right &&
    verifyBST left && verifyBST right = True
  | otherwise = False

maior Vazio = 0
maior (No x left right)
  | x > ml && x > mr = x
  | ml < mr = mr
  | otherwise = ml
  where ml = maior left
        mr = maior right

menor Vazio = 9999
menor (No x left right)
  | x < ml && x < mr = x
  | ml < mr = ml
  | otherwise = mr
  where ml = menor left
        mr = menor right


-- Util
leftmost (No x Vazio _ ) = x
leftmost (No _ ae _ ) = leftmost ae

rightmost (No x _ Vazio) = x
rightmost (No _ _ ad) = rightmost ad

-- Verify if tree is binary search tree
abb Vazio = True
abb (No x Vazio Vazio) = True
abb (No x Vazio ad) = x < (menor ad) && abb ad
abb (No x ae Vazio) = x > (maior ae) && abb ae
abb (No x ae ad) = x > (maior ae) && x < (menor ad) && (abb ae) && (abb ad)

-- Insert item in binary search tree
insere a Vazio = (No a Vazio Vazio)
insere a (No x ae ad)
  | a < x = (No x (insere a ae) ad)
  | otherwise = (No x ae (insere a ad))

-- Remove item from binary search tree
remove a Vazio = Vazio
remove a (No x ae ad)
  | a == x = removeRaiz a (No x ae ad)
  | a < x = (No x (remove a ae) ad)
  | otherwise = (No x ae (remove a ad))

removeRaiz _ (No _ Vazio Vazio) = Vazio
removeRaiz a (No x ae ad)
  | ae == Vazio = (No lm (remove lm ad) ad)
  | otherwise = (No rm ae (remove rm ae))
  where rm = rightmost ae
        lm = leftmost ad

-- Maximum depth of binary search tree
profundidade Vazio = 0
profundidade (No x ae ad) =
  1 + max (profundidade ae) (profundidade ad)

-- Converte uma abb em ordem infixa (ae, no, ad)
tree2infix Vazio = []
tree2infix (No x ae ad) = (tree2infix ae) ++ [x] ++ (tree2infix ad)

tree2prefix Vazio = []
tree2prefix (No x ae ad) = (x:(tree2prefix ae)) ++ (tree2prefix ad)

list2tree l = list2tree' Vazio l
              where list2tree' t [] = t
                    list2tree' t (x:xs) = list2tree' (insere x t) xs

isBalanced Vazio = True
isBalanced (No _ Vazio Vazio) = True
isBalanced (No x ae ad) = (isBalanced ae) && (isBalanced ad)
                        && (profundidade ae == profundidade ad)
-- infix2tree
getItem 1 (x:xs) = x
getItem i (x:xs) = getItem (i-1) xs

-- sorted2balanced l = sorted2balanced
-- list2balanced l = list2balanced

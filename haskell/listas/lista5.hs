
-- tamanho de uma lista
-- soma dos elementos de uma lista
-- soma dos números pares de uma lista
-- soma dos elementos nas posições pares da lista ( o primeiro elemento esta na posicao 1)
-- existe item na lista (True ou False)
-- posição do item na lista (0 se nao esta la, 1 se é o primeiro)
-- conta quantas vezes o item aparece na lista (0 se nenhuma)
-- maior elemento de uma lista - FAZER p/ proxima aula - variáveis locais
-- reverte uma lista - FAZER p/ próxima aula - recursão com acumulados
-- intercala 2 listas (intercala1 e intercala2)


tamanho xs = foldl (\acc _ -> acc + 1) 0 xs
tamanho' xs = foldr (\_ acc -> acc + 1) 0 xs

tamanho'' xs = sum (map (\_ -> 1) xs)

soma xs = foldl (\acc x -> acc + x) 0 xs

somaPar xs = sum(filter even xs)
-- tamanho' xs = map (\x -> )

conta x xs = length $ filter (==x) xs
conta' x xs = foldl (\acc it -> if x == it then acc + 1 else acc) 0 xs

xchg v n xs = map (\x -> if x == v then n else x) xs

removeN x n xs = fst $ foldl (\(li, nn) it -> if x == it && nn > 0 then (li, nn-1)
                                        else (li ++ [it], nn)) ([], n) xs


transposta [[], _] = []
transposta m = (foldl (\acc (x:xs) -> acc ++ [x]) [] m):
              (transposta (foldl (\acc (x:xs) -> acc ++ [xs]) [] m))

transp ([]:_) = []
transp m = col1 : transp restmat
           where col1 = map head m
                 restmat = map tail m
--   foldl (\(mT, m') row ->
-- map (\row -> )

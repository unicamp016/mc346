% Ex. 1 - Tamanho de uma lista.
tamanho([], 0).
tamanho([_|T], L) :- tamanho(T, LL), L is LL+1.

accTamanho([], A, A).
accTamanho([_|T], Acc, L) :- A is Acc + 1, accTamanho(T, A, L).

% Ex. 2 - Soma dos elementos de uma lista (+LIST, -SOMA)
soma(L, R) :- accSoma(L, R, 0).

accSoma([], S, S).
accSoma([H|T], R, S) :- SS is H + S, accSoma(T, R, SS).

% Ex. 3 - Soma dos números pares de uma lista (+LISTA, -SOMA).
somaPar(L, R) :- accSomaPar(L, 0, R).

% Caso base
accSomaPar([], A, A).

% Caso o elemento seja par
accSomaPar([H|T], Acc, R) :-
  Par is H mod 2, Par = 0,
  A is Acc + H,
  accSomaPar(T, A, R).

% Caso o elemento seja ímpar
accSomaPar([H|T], Acc, R) :-
  Par is H mod 2, Par = 1,
  accSomaPar(T, Acc, R).

% Ex. 4 - Soma dos elementos nas posições pares de um lista (+LISTA, -SOMA).
somaPosPar(L, R) :- accSomaPosPar(L, 0, R).

accSomaPosPar([_, H|T], Acc, R) :-
  A is Acc + H,
  accSomaPosPar(T, A, R).

accSomaPosPar([_|[]], A, A).
accSomaPosPar([], A, A).


% Ex. 5 - Existe item na lista (+IT, +LISTA).
existeElem([H|T], IT) :- H = IT; existeElem(T, IT).

% Ex. 6 - Posição do item na lista - 1 se é o primeira e falha se não estiver (+IT, +LISTA, -POS)
posElem(IT, L, Pos) :- accPosElem(IT, L, 1, Pos).

accPosElem(IT, [H|_], Acc, Pos) :-
  IT = H,
  Pos is Acc.

accPosElem(IT, [H|T], Acc, Pos) :-
  IT \= H,
  A is Acc + 1,
  accPosElem(IT, T, A, Pos).

% Ex.7 - Conta quantas vezes um item aparece na lista. (+IT, +LISTA, -CONTA).
contarElem(IT, L, R) :- accContaElem(IT, L, 0, R).

accContaElem(_, [], A, A).
accContaElem(IT, [H|T], Acc, R) :-
  IT = H,
  A is Acc + 1,
  accContaElem(IT, T, A, R).

accContaElem(IT, [H|T], Acc, R) :-
  IT \= H,
  accContaElem(IT, T, Acc, R).

% Ex.8 - Maior elemento de uma lista (+LISTA, -MAX)
maxElem([H|T], R) :- maxElem(T, H, R).

maxElem([], A, A).
maxElem([H|T], Acc, R) :-
  (H > Acc -> maxElem(T, H, R); maxElem(T, Acc, R)).

% Ex. 9 - Reverte uma lista (+LISTA, -LISTA_REV).
reverter([X|Xs], R) :- reverter(Xs, [X], R).
reverter([X|Xs], Ys, R) :- reverter(Xs, [X|Ys], R).
reverter([], Ys, Ys).

reverter2([X|Xs], R) :- reverter2(Xs, [X], R).
reverter2([H|T], A, R) :- reverter2(T, [H|A], R).
reverter2([], A, A).


% Ex. 10 - Intercala duas listas (+LISTA1, +LISTA2, +LISTA_INTER).
intercala([], [], []).
intercala([X|Xs], [Y|Ys], R) :- intercala(Xs, Ys, A), R = [X,Y|A].

% Ex. 11 - Verifica se a lista está ordenada (+LISTA1).
ordenada([]).
ordenada([_|[]]).
ordenada([X1, X2|Xs]) :-
  X1 =< X2,
  ordenada([X2|Xs]).

% Ex. 12 - Dado n, gera lista de 1 a n
gerarLista(N, R) :- gerarLista(N, [], R).
gerarLista(0, A, A) :- !.
gerarLista(N, A, R) :-
  N > 0, % Prevents infinite loop in case of user initializing R with an improper value
  NN is N-1,
  gerarLista(NN, [N|A], R).

% Ex. 13 - Retorna o último elemento de uma lista
ultimo([X|[]], X) :- !.
ultimo([_|Xs], R) :- ultimo(Xs, R).

% Ex. 14 - Retorna a lista sem o ultimo elemento
listaSemUlt([_|[]], []) :- !.
listaSemUlt([X|Xs], R) :- listaSemUlt(Xs, A), R = [X|A].

% Ex. 15 - Shift right
% Chamamos uma função auxiliar que tem como objetivo colocar um elemento no final da lista
shiftR(L, R) :-
  todosMenosUltimo(L, H),
  ultimo(L, U),
  append([U], H, R).

todosMenosUltimo([_], []).
todosMenosUltimo([X|Xs], R) :- todosMenosUltimo(Xs, A), R = [X|A].

% Ex. 16 - Shift righ n vezes
shiftRN(L, 0, L).
shiftRN(L, N, R) :-
  N > 0,
  NN is N -1,
  shiftR(L, NL),
  shiftRN(NL, NN, R).

% Ex. 17 - Shift left
shiftL([X|Xs], R) :- shiftL(X, Xs, R).
shiftL(X, [], [X]).
shiftL(X, [Y|Ys], R) :- shiftL(X, Ys, A), R = [Y|A].

% Ex. 18 - Shift left n vezes
shiftLN(L, 0, L).
shiftLN(L, N, R) :-
  N > 0,
  NN is N -1,
  shiftL(L, NL),
  shiftLN(NL, NN, R).

% Ex. 19 - Remove item da lista
remover(_, [], []).
remover(X, [X|Xs], Xs).
remover(IT, [X|Xs], R) :- remover(IT, Xs, A), R = [X|A].

% Ex. 20 - Reomve item da lista todas as vezes
removerTodos(_, [], []).
removerTodos(IT, [X|Xs], R) :-
  IT = X,
  removerTodos(IT, Xs, R).
removerTodos(IT, [X|Xs], R) :-
  IT \= X,
  removerTodos(IT, Xs, A),
  R = [X|A].

% Ex. 21 - Remove item da lista n primeiras vezes
removerN(_, 0, L, L).
removerN(IT, N, L, R) :-
  N > 0,
  remover(IT, L, NL),
  NN is N - 1,
  removerN(IT, NN, NL, R).

% Ex. 22 - Remover última ocorrencia do item.
removerUltimo(IT, L, R) :-
  reverter(L, Revert),
  remover(IT, Revert, Res),
  reverter(Res, R).

% Ex. 23 - Troca velho por novo na lista.
trocar(_, _, [], []).
trocar(V, N, [V|Vs], [N|Vs]).
trocar(V, N, [X|Xs], R) :- trocar(V, N, Xs, A), R = [X|A].

% Ex. 24 - Troca velho por novo todas as vezes
trocarTodos(_, _, [], []).
trocarTodos(V, N, [X|Xs], R) :-
  V \= X,
  trocarTodos(V, N, Xs, A),
  R = [V|A].
trocarTodos(V, N, [X|Xs], R) :-
  V = X,
  trocarTodos(V, N, Xs, A),
  R = [N|A].

% Ex. 25 - Troca velho por novo n vezes
trocarN(_, _, 0, L, L).
trocarN(Velho, Novo, N, L, R) :-
  N > 0,
  trocar(Velho, Novo, L, NL),
  NN is N - 1,
  trocarN(Velho, Novo, NN, NL, R).

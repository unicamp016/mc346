% Ex 1. - Acha um item em uma árvore binária
% encontrarElem(X, vazio)
encontrarElem(X, arv(X, _, _)) :- !.
encontrarElem(X, arv(N, AE, AD)) :- X < N -> encontrarElem(X, AE); encontrarElem(X, AD).

% Ex. 2 - Verifia se uma árvore é ABB.
verificarABB(arv(_, vazio, vazio)).
verificarABB(arv(X, arv(XE, AEE, AED), vazio)) :-
  X > XE,
  verificarABB(arv(XE, AEE, AED)).
verificarABB(arv(X, vazio, arv(XD, AED, ADD))) :-
  X < XD,
  verificarABB(arv(XD, AED, ADD)).
verificarABB(arv(X, arv(XE, AEE, ADE), arv(XD, AED, ADD))) :-
  X > XE,
  X < XD,
  verificarABB(arv(XE, AEE, ADE)),
  verificarABB(arv(XD, AED, ADD)).

verifyBT(arv(_, vazio, vazio)) :- !.
verifyBT(arv(X, vazio, AD)) :- AD = arv(XD, _, _), X < XD, !, verifyBT(AD).
verifyBT(arv(X, AE, vazio)) :- AE = arv(XE, _, _), X > XE, !, verifyBT(AE).
verifyBT(arv(X, AE, AD)) :-
  arv(XE, _, _) = AE, arv(XD, _, _) = AD,
  X > XE, !, verifyBT(AE),
  X < XD, !, verifyBT(AD).

% Ex. 3 - Insere um item em uma ABB.
insereABB(X, vazio, arv(X, vazio, vazio)) :- !.
insereABB(IT, arv(X, AE, AD), R) :-
  IT < X -> insereABB(IT, AE, RE), R = arv(X, RE, AD);
            insereABB(IT, AD, RD), R = arv(X, AE, RD).

% Ex. 4 - Remove um item de uma ABB.
maiorABB(arv(X, _, vazio), X) :- !.
maiorABB(arv(_, _, AD), R) :- maiorABB(AD, R), !.

menorABB(arv(X, vazio, _), X) :- !.
menorABB(arv(_, AE, _, R)) :- menorABB(AE, R), !.

removeABB(X, arv(X, vazio, vazio), vazio) :- !.
removeABB(X, arv(X, vazio, AD), R) :-
  menorABB(AD, M),
  removeABB(M, AD, RD),
  R = arv(M, vazio, RD).
removeABB(X, arv(X, AE, AD), R) :-
  maiorABB(AE, M),
  removeABB(M, AE, RE),
  R = arv(M, RE, AD).
removeABB(IT, arv(X, AE, AD), R) :-
  IT < X -> removeABB(IT, AE, RE), R = arv(X, RE, AD);
            removeABB(IT, AD, RD), R = arv(X, AE, RD).

% Ex. 5 - Calcula a profundidade de uma ABB
atualizaProf(PE, PD, R) :-
  PE > PD -> R is PE + 1; R is PD + 1.

profundidadeABB(vazio, 0) :- !.
profundidadeABB(arv(_, AE, AD), R) :-
  profundidadeABB(AE, PE),
  profundidadeABB(AD, PD),
  atualizaProf(PE, PD, R).

% Ex. 7 - Lista infixa a partir de uma ABB.
listaInfixa(vazio, []) :- !.
listaInfixa(arv(X, AE, AD), R) :-
  listaInfixa(AE, RE),
  listaInfixa(AD, RD),
  append(RE, [X|RD], R).

% Ex. 8 - Lista prefixa a partir de uma ABB.
listaPrefixa(vazio, []) :- !.
listaPrefixa(arv(X, AE, AD), R) :-
  listaPrefixa(AE, RE),
  listaPrefixa(AD, RD),
  append(RE, RD, RED),
  R = [X|RED].

% Ex. 9 - Lista para ABB.
lista2ABB([], vazio).
lista2ABB([X|Xs], R) :-
  lista2ABB(Xs, ABB),
  insereABB(X, ABB, R).

% Ex. 10 - Dado um dicionário acesse o valor associado a uma chave, falha se a chave não existe
encontrarValor(_, [], _) :- !, fail.
encontrarValor(K, [dic(K, V)|_], V) :- !.
encontrarValor(K, [_|Ds], R) :- encontrarValor(K, Ds, R).

% Ex. 11 - Insere um par (chave, valor) no dicionário ou troca o valor associado a chave
insereDicElem(KV, [], [KV]) :- !.
insereDicElem(dic(K, V), [dic(K, _)|Ds], [dic(K, V)|Ds]) :- !.
insereDicElem(KV, [D|Ds], R) :-
  insereDicElem(KV, Ds, RD),
  R = [D|RD].

insereKV(K, V, [], [dic(K,V)]) :- !.
insereKV(K, V, [dic(K, _)|Ds], [dic(K, V)|Ds]) :- !.
insereKV(K, V, [D|Ds], R) :-
  insereKV(K, V, Ds, RD),
  R = [D|RD].

% Ex. 12 -  Remove uma chave e seu valor do dicionário
removeChave(_, [], []) :- !.
removeChave(K, [dic(K, _)|Ds], Ds) :- !.
removeChave(K, [D|Ds], R) :-
  removeChave(K, Ds, RD),
  R = [D|RD].

% Ex. 13 - Contador: soma 1 ao valor de uma chave ou adiciona ela no dic.
soma1(K, [], [dic(K, 1)]) :- !.
soma1(K, [dic(K, V)|Ds], R) :-
  !, NV is V + 1,
  R = [dic(K, NV)|Ds].
soma1(K, [D|Ds], R) :-
  soma1(K, Ds, RD),
  R = [D|RD].


insereABB(Chave, _, arv(Chave, _, _, _), _) :- !, fail.
insereABB(Chave, Valor, vazio, arv(Chave, Valor, vazio, vazio)) :- !.
insereABB(Chave, Valor, arv(K, V, AE, AD), ArvNova) :-
  Chave < K -> insereABB(Chave, Valor, AE, RE), ArvNova = arv(K, V, RE, AD);
               insereABB(Chave, Valor, AD, RD), ArvNova = arv(K, V, AE, RD).

lookupABB(_, vazio, _) :- !, fail.
lookupABB(Chave, arv(Chave, Valor, _, _), Valor) :- !.
lookupABB(Chave, arv(K, _, AE, AD), Valor) :-
  Chave < K -> lookupABB(Chave, AE, Valor);
               lookupABB(Chave, AD, Valor).


trocaABB(_, _, vazio, _) :- !, fail.
trocaABB(Chave, ValorNovo, arv(Chave, _, AE, AD), arv(Chave, ValorNovo, AE, AD)) :- !.
trocaABB(Chave, ValorNovo, arv(K, V, AE, AD), ArvNova) :-
  Chave < K -> trocaABB(Chave, ValorNovo, AE, RE), ArvNova = arv(K, V, RE, AD);
               trocaABB(Chave, ValorNovo, AD, RD), ArvNova = arv(K, V, AE, RD).


getdict(Chave, Dict, Valor) :- lookupABB(Chave, Dict, Valor).

xgetdict(Chave, Dict, _, Valor) :- lookupABB(Chave, Dict, Valor), !.
xgetdict(Chave, Dict, ValorDef, ValorDef) :- \+ lookupABB(Chave, Dict, _).


putdic(Chave, Valor, DicVelho, DicNovo) :- insereABB(Chave, Valor, DicVelho, DicNovo).
putdic(Chave, Valor, DicVelho, DicNovo) :- \+ insereABB(Chave, Valor, DicVelho, DicNovo),
                                           trocaABB(Chave, Valor, DicVelho, DicNovo).

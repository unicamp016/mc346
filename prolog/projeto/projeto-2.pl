/** Author: Rafael Figueiredo Prudencio
 *  RA: 186145
 *  Description: We are trying to identify combinations between strands of DNA
 *  in order to reconstruct a genome. Given a list of strings, we print to
 *  standard output all possible combinations between them, assuming four
 *  characters in common are enough for them to be combined.
 */

main :-
	prompt(_, ''), 							% Remove initial prompt in stdout
	read_string(user_input, _, Genome), 				% Read entire file
	split_string(Genome, "\n", "", StrandListRaw),			% Split lines into list of strings
	exclude(==(""), StrandListRaw, StrandListWhite),		% Filter out empty strings from list
	remove_trailing_whitespace(StrandListWhite, StrandList),	% Removes trailing whitespace from each string in list
	generate_genome(StrandList, GenomeList),			% Generate genome with combined strands
	print_list(GenomeList),						% Print one strand per line
	halt.								% Terminate program

/** Generates genome list with all possible strand combinations
  * params:	+StrandList - List of individual strands that we should look to combine.
  *		-GenomeList - List of condensed strands.
  */
generate_genome(StrandList, GenomeList) :-
	all_possible_strands(StrandList, CandidateStrands),
	length(CandidateStrands, L),
	(L =:= 0 -> GenomeList = StrandList;
	update_strand_list(StrandList, CandidateStrands, NewStrandList),
	generate_genome(NewStrandList, GenomeList)).

/** Updates list of strands considering newfound strand combination
 *  params: 	+StrandList 		- List of current individual strands.
 *		+CandidateStrands 	- List of candidate strands to be combined.
 *		-NewStrandList		- List of strands with updated combination.
 */
update_strand_list(StrandList, [strand(Strand1, Strand2, NewStrand)|_], Ret) :-
	delete(StrandList, Strand1, TmpStrandList),
	delete(TmpStrandList, Strand2, TmpStrandList2),
	Ret = [NewStrand|TmpStrandList2].

/**	Finds all possible strand combinations
 *	params: +StrandList 		- List of individual strands that we should look to combine.
 *		-CandidateStrands 	- List of all possible strand combinations.
 */
all_possible_strands(StrandList, CandidateStrands) :-
	all_possible_strands(StrandList, StrandList, [], CandidateStrands).

all_possible_strands([], _, Acc, Acc) :- !.
all_possible_strands([HeadStrand|T], InitStrandList, Acc, CandidateStrands) :-
		select(HeadStrand, InitStrandList, TmpStrandList), 			% List with all strands except current list head
		match_strands(HeadStrand, TmpStrandList, Acc, RetAcc),			% Match current list head to all other strands
		all_possible_strands(T, InitStrandList, RetAcc, CandidateStrands).	% Recursively generate all possible strand combinations

/** Matches a strand to all other strands in a list.
 *  params: 	+Strand 	- Individual strand we want to match to others.
 *		+StrandList 	- List of strands that we want to verify compatibility with.
 * 		-StrandCompList - List of strands composed of (Strand1, Strand2, CombStrand) that match with Strand.
 */
match_strands(_, [], Acc, Acc) :- !.
match_strands(Strand, [TmpStrand|T], Acc, Ret) :-
	% Prep strings for comparison
	string_to_list(Strand, Codes1),
	string_to_list(TmpStrand, Codes2),
	% Count number of compatible codes
	compatible(Codes1, Codes2, Count, CodesRet),
	% Add match to database
	(Count >= 4 -> 	string_to_list(UnifiedStrand, CodesRet),
			TmpAcc = [strand(Strand, TmpStrand, UnifiedStrand)|Acc];
			TmpAcc = Acc),
	match_strands(Strand, T, TmpAcc, Ret).

/** Removes whitespace preceding or proceding strings in list
 *  params: 	+StringListIn - Input string list from which we want to remove whitespaces.
 * 		-StringListOut - Trimmed list of strings.
 */
remove_trailing_whitespace(StringListIn, StringListOut) :- remove_trailing_whitespace(StringListIn, [], StringListOut).
remove_trailing_whitespace([], Acc, Acc) :- !.
remove_trailing_whitespace([H|T], Acc, R) :-
	normalize_space(atom(NormH), H),
	remove_trailing_whitespace(T, [NormH|Acc], R).

/** Verifies compatibility between two strands
 *  params: 	+List1 - List of ASCII codes for strand 1.
 * 		+List2 - List of ASCIi codes for strand 2.
 *		-Count - Number of chars strand have in common.
 *		-Ret - Combination of List1 with List2, in this order.
 */

% Call to auxiliary function with accumulators.
compatible(List1, List2, Count, Ret) :- compatible(List1, List2, List1, List2, 0, [], Count, Ret).

% In case we reach the end of original list without first reaching the end of combinations list.
compatible([], _, _, _, 0, []) :- !.

% Case in which we are at the end of matches list and we need to verify whether ther are enough of them.
compatible(InitList1, _, [], List2, AccCount, AccList1, AccCount, Ret) :-
	!, AccCount >= 4 -> append(AccList1, InitList1, TmpRet), append(TmpRet, List2, Ret);
											Ret = [].

% Case in which we found two equal elements and we want to keep looking for more
compatible(InitList1, InitList2, [H|T1], [H|T2], AccCount, AccList1, Count, Ret) :-
	!, AccCount1 is AccCount + 1,
	compatible(InitList1, InitList2, T1, T2, AccCount1, AccList1, Count, Ret).

% Case in which list heads differ and we need to start looking from initial again.
compatible([InitH|InitT], InitList2, _, _, _, AccList1, Count, Ret) :-
	!, append(AccList1, [InitH], TmpAccList1),
	compatible(InitT, InitList2, InitT, InitList2, 0, TmpAccList1, Count, Ret).

/** Print list of strings to standard output inserting a newline after every item.
 *  params: +List - List of items to print to stdout.
 */
print_list([]).
print_list([H|T]) :- writeln(user_output, H), print_list(T).
